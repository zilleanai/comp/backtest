import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { bindRoutineCreators } from 'actions'
import { InfoBox, PageContent } from 'components'
import { RunBacktest } from 'comps/backtest/components'

class Backtest extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { isLoaded} = this.props
    return (
      <PageContent>
        <Helmet>
          <title>Backtest</title>
        </Helmet>
        <h1>Backtest!</h1>
        <RunBacktest />
      </PageContent>)
  }
}

export default compose(
)(Backtest)
