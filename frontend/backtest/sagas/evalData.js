import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { loadEvalData } from 'comps/evaluate/actions'
import EvaluationApi from 'comps/evaluate/api'
import { selectEvalData } from 'comps/evaluate/reducers/evalData'


export const KEY = 'evalData'

export const maybeLoadEvalDataSaga = function* ({payload}) {
  const { byId, isLoading } = yield select(selectEvalData)
  const isLoaded = !!byId[payload.id]
  if (!(isLoaded || isLoading)) {
    yield put(loadEvalData.trigger(payload))
  }
}

export const loadEvalDataSaga = createRoutineSaga(
  loadEvalData,
  function* successGenerator(payload) {
    var data = yield call(EvaluationApi.loadEvalData, payload)
    yield put(loadEvalData.success(data))
  }
)

export default () => [
  takeEvery(loadEvalData.MAYBE_TRIGGER, maybeLoadEvalDataSaga),
  takeLatest(loadEvalData.TRIGGER, loadEvalDataSaga),
]
