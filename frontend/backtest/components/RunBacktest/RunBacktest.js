import React from 'react'
import { compose } from 'redux'
import './run-backtest.scss'
import { v1 } from 'api'
import { get } from 'utils/request'
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { storage } from 'comps/project'
import { ResultData, TagSelect } from 'comps/backtest/components'

registerPlugin(FilePondPluginImagePreview);


function backtest(uri) {
  return v1(`/backtest${uri}`)
}

class RunBacktest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // Set initial files
      priceTag: 'price',
      threshold: 0.25,
      files: [],
      results: [],
      resultids: []
    };
  }

  handlePriceTagChange = ({ value }) => {
    this.setState({ priceTag: value });
  }

  handleInit() {
  }

  addResult = (res) => {
    const result = {
      id: res['id'],
    }
    const results = this.state.results
    const resultids = this.state.resultids
    results.push(result)
    resultids.push(result.id)
    this.setState({ results: results, resultids: resultids })
  }

  render() {
    const { priceTag, threshold, results, resultids } = this.state
    const server = {
      url: backtest(`/backtest/${storage.getProject()}/file/${threshold}/${priceTag}`),
      timeout: 160000,
      process: {
        onload: (res) => {
          const res_obj = JSON.parse(res)
          this.addResult(res_obj)
          return res;
        }
      }
    }

    return (
      <div className="RunBacktest">
        <label>Price Tag:</label>
        <TagSelect
          defaultValue={[priceTag]}
          onChange={this.handlePriceTagChange} />
        <label>Trade threshold:</label>
        <input
          type="number"
          step="0.1"
          value={this.state.threshold}
          onChange={e => {
            this.setState({ threshold: parseFloat(e.target.value, 10) })
          }
          }
        />

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          maxFiles={100}
          server={server}
          oninit={() => this.handleInit()}
          onupdatefiles={(fileItems) => {
            // Set current file objects to this.state

            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}


        </FilePond>
        <button type='button' onClick={() => {
          get(v1(`/backtest/backtest/${storage.getProject()}`)).then((res) => {
            this.addResult(res)
          })
        }} >Backtest</button>
        <label>results:</label>
        <ul className="results">
          {results.map((result, i) => {
            return (
              <li key={i} >
                <ResultData id={result['id']} />
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default compose(
)(RunBacktest)
