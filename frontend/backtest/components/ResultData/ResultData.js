import React from 'react'
import Iframe from 'react-iframe'
import { compose } from 'redux'
import './result-data.scss'
import { v1 } from 'api'

class ResultData extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      reloadStr: ''
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    const { isLoaded } = this.props
    if (!isLoaded) {
      this.timeout = setTimeout(() => {
        this.setState({ isLoaded: true })
      }, 5000);
    }
  }

  render() {
    const { id, error } = this.props
    console.log(id)
    const { isLoaded, reloadStr } = this.state
    if (!isLoaded || error) {
      return (<button onClick={()=>{this.setState({ isLoaded: !isLoaded })}}>Reload</button>)
    }
    return (
      <div className='result-data'>
      <Iframe url={v1(`/backtest/download/${id}.html?reloadStr=${reloadStr}`)}
        width="100%"
        height="800px"
        id={id}
        display="initial"
        position="relative"/>
        <a className='download' href={v1(`/backtest/download/${id}.html`)} ><button>Download</button></a>
        <button className='reload' onClick={()=>{this.setState({ reloadStr: ""+(new Date()).getSeconds()})}}>Reload</button>
      </div>
    )
  }
}
export default compose(
)(ResultData)
