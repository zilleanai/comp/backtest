import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function evaluate(uri) {
  return v1(`/evaluate${uri}`)
}

export default class Evaluate {
  static loadEvalData(evalData) {
    return get(evaluate(`/evaluation/${evalData.id}`))
  }
}
