import { loadEvalData } from 'comps/evaluate/actions'


export const KEY = 'evalData'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function(state = initialState, action) {
  const { type, payload } = action
  const { ids, byId } = state

  switch (type) {
    case loadEvalData.REQUEST:
      return { ...state,
        isLoading: true,
      }

    case loadEvalData.SUCCESS:
      if (!ids.includes(payload.id)) {
        ids.push(payload.id)
      }
      byId[payload.id] = payload
      return { ...state,
        ids,
        byId,
      }

    case loadEvalData.FULFILL:
      return { ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectEvalData = (state) => state[KEY]
export const selectEvalDataById = (state, id) => selectEvalData(state).byId[id]
