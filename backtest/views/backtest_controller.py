import os
import json
import pickle
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
import zipfile
import uuid

from werkzeug.utils import secure_filename
from ..tasks import backtest_async_task


class BacktestController(Controller):

    def backtest_task(self, project, data, threshold, tag):
        id = str(uuid.uuid4())+''
        if data:
            data = data.read()
        backtest_async_task(project, id, data, threshold, tag)
        return id

    @route('/download/<string:id>')
    def download(self, id):
        redis = AppConfig.SESSION_REDIS
        cached = redis.get(id)
        if not cached:
            return abort(404)
        return cached

    @route('/backtest/<string:project>/file/<float:threshold>/<string:tag>', methods=['POST'])
    def backtest_file(self, project, threshold, tag):
        if 'filepond' not in request.files:
            return abort(404)
        file = request.files['filepond']
        if file.filename == '':
            return abort(404)
        id = self.backtest_task(project, file.stream, threshold, tag)
        return jsonify({'id': id, 'project': project, 'filename': file.filename})

