import os
import gc
import json
from io import StringIO, BytesIO
import pickle
import multiprocessing
import pandas as pd
import numpy as np

from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

from zworkflow import Config
from zworkflow.dataset import get_dataset
from zworkflow.preprocessing import get_preprocessing
from zworkflow.model import get_model
from zworkflow.evaluate import get_evaluate
from zworkflow.predict import get_predict

import subprocess

backtester = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'backtester.py')

@celery.task(serializer='pickle')
def backtest_async_task(project, id, data, threshold, tag):
    multiprocessing.current_process()._config['daemon'] = False
    os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))

    bdata = BytesIO(data)
    bdata.seek(0)
    
    df = pd.read_csv(bdata)

    configfile = os.path.join('workflow.yml') or {}
    config = Config(configfile)
    config['general']['verbose'] = False

    model = get_model(config)
    if config['general']['verbose']:
        print(model)
    preprocessing = get_preprocessing(config)
    dataset = get_dataset(config, preprocessing, data=data)
    predict = get_predict(config)
    data_io = BytesIO()
    data_io.write(predict.predict(dataset, model))
    data_io.seek(0)

    df_action = pd.read_csv(data_io)
    df_action[df_action>threshold] = 1
    df_action[df_action<-threshold] = -1
    df_action[(df_action > -1) & (df_action < 1)] = 0
    df['Open'] = df['High'] = df['Low'] = df['Close'] = df[tag]
    df['action'] = df_action['action']
    df.to_csv('backtesting.csv')

    subprocess.call(['python3', backtester])

    result = {}
    result['id'] = id
    redis = AppConfig.SESSION_REDIS
    if not data is None:
        redis.setex(''+id+'_orig', 1800, data)
    with open('backtesting.html', "rb") as f:
        data = f.read()
        result['html'] = ''+id+'.'+'html'
        redis.setex(''+id+'.'+'html', 1800, data)
    
    redis.setex(id, 1800, json.dumps(result))
