import pandas as pd
from backtesting import Backtest
from backtesting.lib import SignalStrategy

class BacktestStrategy(SignalStrategy):

    def init(self):
        super().init()
        signal = pd.Series(self.data['action']).fillna(0).astype(int)
        self.set_signal(signal)

df = pd.read_csv('backtesting.csv')
bt = Backtest(df, BacktestStrategy, cash=1000, commission=.0)

output = bt.run()
bt.plot(filename='backtesting.html')